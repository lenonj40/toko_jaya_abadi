 <html>
<head>
<meta charset="utf-8">
<title>Master</title>
<style type="text/css">
body ul li {
	font-family: Arial, Helvetica, sans-serif;
	font-size:16px;
	list-style-type: none;
	text-align:center;
	vertical-align:central;
	margin-left:65px;
}
body ul li ul {
	padding: 0px;
	display: none;
	text-align:left;
}
body ul li {
	background-color: #CCC;
	float: left;
	height: 40px;
	width: 150px;
	float:left;
	line-height:40px;
}
body ul li:hover ul {
	display:block;
}
body ul li:hover{
	background-color: #F00;
	top: 20px;
	right: 500px;
	bottom: 50px;
	vertical-align: central;
	text-align: center;
}

body menu {
	font-size: 10px;
}
#menu {
	background-color: #F00;
}
</style>
</head>
<body>
<form action="<?=base_url()?>barang/inputbarang" method="POST">
<table width="100%" border="0" align="center">
<tr>
    <td align="center" bgcolor="#006699"><h1>&quot;TOKO JAYA ABADI&quot;</h1></td>
  </tr>
    <td width="100" height="50" align="center" valign="middle" bgcolor="#006699"><ul id="menu" name="menu">
       	<li>Home</li>
            <li>Master
            	<ul>
                	<li><a href="<?=base_url();?>karyawan/listkaryawan">Data Karyawan</a></li>
                    <li><a href="<?=base_url();?>jabatan/listjabatan">Data Jabatan</a></li>
                    <li><a href="<?=base_url();?>barang/listbarang">Data Barang</a></li>
                    <li><a href="<?=base_url();?>supplier/listsupplier">Data Supplier</a></li>
              </ul>
        </li>
            <li>Transaksi</li>
            <li>Report</li>
            <li>Log Out</li>
    </ul></td>
  <tr>
    <td><table width="100%" border="0">
    <tr align="center">
        <td height="10"><h2>&nbsp;</h2></td>
      </tr>
      <tr align="center">
        <td height="80"><h2>Input Barang</h2></td>
      </tr>
      <tr>
        <td><table width="50%" border="0" cellspacing="0" cellpadding="5" align="center" bgcolor="">
          <tr>
            <td width="37%">Kode Barang</td>
            <td width="4%"> :</td>
            <td width="59%"><input type="text" name="kode_barang" id="kode_barang" maxlength="5"></td>
          </tr>
          <tr>
            <td>Nama Barang</td>
            <td>:</td>
            <td><input type="text" name="nama_barang" id="nama_barang" maxlength="150"></td>
          </tr>
          <tr>
            <td>Harga</td>
            <td>:</td>
            <td><input type="text" name="harga_barang" id="harga_barang" maxlength="100"></td>
          </tr>
          <tr>
            <td>Jenis Barang</td>
            <td>:</td>
            <td><select name="nama_jenis" id="nama_jenis">
              <?php foreach($jenis_barang as $data) { ?>
              <option value="<?=$data->kode_jenis;?>"><?=$data->nama_jenis;?></option>
              <?php } ?>
              </select></td>
          </tr>
          <tr>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td><input type="submit" name="Submit" id="Submit" value="Simpan">
              <input type="reset" name="reset2" id="reset2" value="Reset"></td>
          </tr>
          <tr>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td><a href="<?=base_url();?>barang"><input type="button" name="Kembali" id="Kembali" value="Kembali Ke Menu Sebelumnya"></a></td>
          </tr>
        </table></td>
      </tr>
    </table></td>
  </tr>
</table>
</form>
</body>
</html>
