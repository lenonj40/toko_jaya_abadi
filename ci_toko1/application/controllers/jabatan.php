 <?php defined('BASEPATH') OR exit('No direct script access allowed');


class jabatan extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
		//load model terkait
		$this->load->model("jabatan_model");
	}

	public function index()
	{
		$this->listJabatan();
	}
	
	public function listJabatan()
	{
		$data['data_jabatan'] =$this->jabatan_model->tampilDataJabatan();
		$this->load->view('list_jabatan',$data);
	}
	
	public function detailJabatan($kode_jabatan)
	{
		$data['detailJabatan']=$this->jabatan_model->detailJabatan($kode_jabatan);
		$this->load->view('detailJabatan',$data);
	}
	
	public function inputJabatan()
	{
		$data['data_jabatan']=$this->jabatan_model->tampilDataJabatan();
			if(!empty($_REQUEST)){
				$m_jabatan=$this->jabatan_model;
				$m_jabatan->saveDataJabatan();
				redirect("jabatan/index","refresh");
				}
		
		$this->load->view('input_jabatan',$data);
	}	
}
