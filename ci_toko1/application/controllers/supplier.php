 <?php defined('BASEPATH') OR exit('No direct script access allowed');


class supplier extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
		//load model terkait
		$this->load->model("supplier_model");
	}

	public function index()
	{
		$this->listSupplier();
	}
	
	public function listSupplier()
	{
		$data['data_supplier'] =$this->supplier_model->tampilDataSupplier();
		$this->load->view('list_supplier',$data);
	}
	
	public function detailSupplier($kode_supplier)
	{
		$data['detailSupplier']=$this->supplier_model->detailSupplier($kode_supplier);
		$this->load->view('detailSupplier',$data);
	}	
	
	public function inputSupplier()
	{
		$data['data_supplier']=$this->supplier_model->tampilDataSupplier();
			if(!empty($_REQUEST)){
				$m_supplier=$this->supplier_model;
				$m_supplier->saveDataSupplier();
				redirect("supplier/index","refresh");
				}
		
		$this->load->view('input_supplier',$data);
	}
}
