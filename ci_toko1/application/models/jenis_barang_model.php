 <?php defined('BASEPATH') OR exit('No direct script access allowed');


class jenis_barang_model extends CI_Model
{
	//panggil nama table
	private $_table="jenis_barang";
	
	public function tampilDataJenisBarang()
	{
		// seperti : select * from <>nama_table>
		return $this->db->get($this->_table)->result();
	}
	
	public function saveDataJenisBarang()
	{
		$data['kode_jenis']	=$this->input->post('kode_jenis');
		$data['nama_jenis']	=$this->input->post('nama_jenis');
		$data['flag']	=1;
		$this->db->insert($this->_table,$data);
	}
}
